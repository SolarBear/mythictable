﻿import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import EntityStore from './store/EntityStore';
import LivePlayState from './live/LivePlayState';

Vue.use(VueRouter);
Vue.use(Vuex);

const LivePlay = () => import('./LivePlay.vue');

const store = new Vuex.Store({
    modules: {
        entities: EntityStore,
        live: LivePlayState
    },
    state: {
    }
});

const router = new VueRouter({
    routes: [
        { path: '*', redirect: '/play' },
        { path: '/play/:sessionId/:sceneId', component: LivePlay, props: true },
        { path: '/play', redirect: '/play/$demo/stronghold' },
        { path: '/$debug/*', component: () => import('./debug/DebugPage.vue') },
        { path: '/$debug', redirect: '/$debug/' },
    ],
    mode: 'history'
})

let app = new Vue({
    el: '#app',
    store,
    router,
    render: h => h('router-view')
});
